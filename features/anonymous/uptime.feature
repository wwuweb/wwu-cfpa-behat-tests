#features/anonymous/uptime.feature

Feature: Uptime check
  Visit the front page and verify that the response code is 200

  Background:
    Given I am at "/"

  Scenario: The front page of cfpa.wwu.edu should contain the department name
    Then the response status code should be 200
