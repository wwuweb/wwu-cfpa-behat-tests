<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Exception\PendingException;

use Drupal\DrupalExtension\Context\RawDrupalContext;

class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {}
